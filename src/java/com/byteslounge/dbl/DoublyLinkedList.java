/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byteslounge.dbl;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fx62
 */
public class DoublyLinkedList {

    private Node first;
    private Node last;
    private int size = 0;

    public DoublyLinkedList() {
        this.first = this.last = null;
    }

    public boolean isEmpty() {
        return first == null && last == null;
    }

    public void addLeft(Object data) {
        Node node = new Node(data);
        if (isEmpty()) {
            first = node;
        } else {
            if (last == first) {
                first.setLeft(node);
            } else {
                last.setLeft(node);
            }
            node.setRight(last);
        }
        last = node;
        size++;
        //System.out.println("First: " + first.getLeft() + "   " + first + "   " + first.getRight());
        //System.out.println("Last: " + last.getLeft() + "   " + last + "   " + last.getRight());
    }

    public void addRight(Object data) {
        Node node = new Node(data);
        if (isEmpty()) {
            last = node;
        } else {
            if (last == first) {
                last.setRight(node);
            } else {
                first.setRight(node);
            }
            node.setLeft(first);
        }
        first = node;
        size++;
    }

    public Node deleteLeft() {
        Node deleted = null;
        if (!isEmpty()) {
            size--;
            deleted = last;
            last = last.getRight();
            if (last != null) {
                last.setLeft(null);
            } else {
                deleted = first;
                first = null;
            }
        } else{
            size = 0;
        }
        return deleted;
    }

    public Node deleteRight() {
        Node deleted = null;
        if (!isEmpty()) {
            size--;
            deleted = first;
            first = first.getLeft();
            if (first != null) {
                first.setRight(null);
            } else {
                last = null;
            }
        } else{
            size = 0;
        }
        return deleted;
    }

    public int forceSize() {
        Node temp = last;
        int tempInt = 0;
        if (!isEmpty()) {
            first.setRight(null);
            while (temp != null) {
                //System.out.print(temp.getData().getId() + " - " + temp.getData().getNombre() + " -> ");
                //System.out.print(temp.getData() + " -> ");
//                System.out.print("(left= " + temp.getLeft() + ")");
                tempInt++;
                temp = temp.getRight();
            }
            //System.out.println();
        }
        return tempInt;
    }
    
    public List<Object> showFromLeft() {
        Node temp = last;
        List<Object> students = new ArrayList<>();
        if (!isEmpty()) {
            first.setRight(null);
            while (temp != null) {
                students.add(temp.getData());
                //System.out.print(temp.getData().getId() + " - " + temp.getData().getNombre() + " -> ");
                System.out.print(temp.getData() + " -> ");
//                System.out.print("(left= " + temp.getLeft() + ")");
                temp = temp.getRight();
            }
            System.out.println();
            return students;
        }
        return null;
    }

    public List<Object> showFromRight() {
        Node temp = first;
        List<Object> students = new ArrayList<>();
        if (!isEmpty()) {
            last.setLeft(null);
            while (temp != null) {
                //System.out.print(temp.getData().getId() + " - " + temp.getData().getNombre() + " -> ");
                System.out.print(temp.getData() + " -> ");
                students.add(temp.getData());
//                System.out.print("(left= " + temp.getLeft() + ")");
                temp = temp.getLeft();
            }
            System.out.println();
            return students;
        }
        return null;
    }

    public Node getFirst() {
        return first;
    }

    public void setFirst(Node first) {
        this.first = first;
    }

    public Node getLast() {
        return last;
    }

    public void setLast(Node last) {
        this.last = last;
    }

    public int getSize() {
        return size;
    }

    /*public static void main(String args[]) {
        DoublyLinkedList dlist = new DoublyLinkedList();
        for(byte i = 0; i < 40; i++){
            dlist.addLeft(i);
        }
        while(!dlist.isEmpty()){
            dlist.deleteLeft();
            System.out.println(dlist.getSize());
        }
        System.out.println("*****");
        for(byte i = 0; i < 40; i++){
            dlist.addLeft(i);
        }
        while(!dlist.isEmpty()){
            dlist.deleteRight();
            System.out.println(dlist.getSize());
        }
    }*/
}
