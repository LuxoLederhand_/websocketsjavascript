/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byteslounge.players;

import javax.websocket.Session;

/**
 *
 * @author fx62
 */
// cada session generara un objeto de tipo player para saber que lista esta enlazada a la sesion
public class Player {
    private String id;
    private int cards;
    private Session pSession;

    public Player(String id, Session pSession){
        this.id = id;
        this.pSession = pSession;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCards() {
        return cards;
    }

    public void setCards(int cards) {
        this.cards = cards;
    }

    public Session getpSession() {
        return pSession;
    }

    public void setpSession(Session pSession) {
        this.pSession = pSession;
    }
}
