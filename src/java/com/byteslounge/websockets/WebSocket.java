package com.byteslounge.websockets;
//kill me please

import com.byteslounge.dbl.DoublyLinkedList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import com.byteslounge.players.Player;
import com.byteslounge.spl.LinkedList;
import com.byteslounge.spl.Node;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket")
public class WebSocket {

    // cartas ordenadas
    private static ArrayList<String> temp = new ArrayList<>(Arrays.asList(
            "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9",
            "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9",
            "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
            "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9"));
    // lista simple general que se utilizara como una pila
    private static LinkedList list = new LinkedList();
    // llevar el control de sessiones
    private static final Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());
    // Hash set de sessiones
    private static DoublyLinkedList list1 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 1
    private static DoublyLinkedList list2 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 2
    private static DoublyLinkedList list3 = new DoublyLinkedList();
    // lista doblemente enlazada de carta del jugador 3
    private static ArrayList<Player> players = new ArrayList<>();
    private static byte turn = 0;//Cantidad de veces que se le dio click al button de manotazo
    //private static ArrayList<String> orden = new ArrayList<>();
    //lleva los segundos
    private static String turnCountdown = "";
    //Es el que se utiliza para validar el turno. 
    //hace posible que una persona no pueda dar Click al boton de turno si no es su turno
    private static String clicker = "";
    //Es el numero de la carta
    private static String n = "";
    //El mensaje de cuantos jugadores conectados
    private static String player = "";
    //Usa la logica de quien es el ganador, llevando la cuenta de cuantas cartas se han quitado
    private static int gave1;//cartas entregadas por Jugador 1
    private static int gave2;//cartas entregadas por Jugador 2
    private static int gave3;//cartas entregadas por Jugador 3
    //ño
    private static int my = 1;//Turno del jugador
    private static int over = 0;////Its over 90000. ID del juego dentro del servidor JAVA

    ;

    // genera numeros random para generar un nuevo orden en cada juego
    public void fillCards() {
        while (!temp.isEmpty()) {
            byte rand = (byte) (Math.random() * (temp.size()));
            list.add(temp.remove(rand));
        }
        //list.show();
    }

    // se ejecuta cada vez que un nuevo jugador inicia y al presionar el boton movement
    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {
        //String part = "";
        int playerFail = 0;//Almacena el numero del jugador que pierde
        int ptr = 1;//Recibe el numero de jugador que recibira las cartas al perder
        for (Player pl : players) {
            if (session == pl.getpSession()) {
                ptr = pl.getCards();
            }
        }
        if (message.substring(1).equals("hand")) {//MANOTAZOOOOOOOOOOOOOOOOOOOOOOO!!!!
            //System.out.println(message.charAt(0) + " ++++  " + n.charAt(1));
            turn++;
            int i = (int) message.charAt(0);//ID del numero que juega
            int deleted = (int) n.charAt(1);//Carta que juega
            //System.out.println(i + " ----  " + deleted);
            //Sancion al jugador que dio click de último
            if (turn == 3) {
                turn = 0;//Reinicia turno
                for (Player pl : players) {
                    if (pl.getpSession() == session) {
                        playerFail = fool(pl.getCards());//cosita wonita bbsita brrrr
                    }
                }
            } else if (i != deleted) {//4x4 
                turn = 0;
                for (Player pl : players) {
                    if (pl.getpSession() == session) {
                        playerFail = fool(pl.getCards());
                    }
                }
                //System.out.println("******");
            }
            synchronized (clients) {//Manda el mensaje resultante separado por "," para el Javascript
                for (Session client : clients) {
                    client.getBasicRemote().sendText(showPlayersConected(clients.size()) + "," + !list1.isEmpty()
                            + "," + !list2.isEmpty() + "," + !list3.isEmpty() + "," + list1.forceSize() + "," + list2.forceSize()
                            + "," + list3.forceSize() + "," + n + "," + list.forceSize() + "," + turn + "," + my + "," + "" + "," + playerFail);
                }
                //System.out.println("La ultima sesion fue :" + session.getId() + " del jugador " + pl.getCards());
            }
        } else if (message.equals("card")) {//Muestra la carta de manera elegante
            turn = 0;
            for (Player pl : players) {//Valida el click para que coincida de la sesion para que el unico que pueda
                //jugar en ese turno es el designado para ese turno.
                //System.out.println("***  " + clicker + "    " +  pl.getId());
                if (clicker.equals(pl.getId())) {
                    clicker = "";
                    //System.out.println(session.getId() + "    " + pl.getId());
                    n = "";
                    com.byteslounge.dbl.Node crd;//oa soi un nodo kawaii
                    try {//Por si las moscas
                        switch (pl.getCards()) {
                            case 1:
                                crd = list1.deleteLeft();
                                n = crd.getData().toString();
                                gave1++;
                                break;
                            case 2:
                                crd = list2.deleteLeft();
                                n = crd.getData().toString();
                                gave2++;
                                break;
                            case 3:
                                crd = list3.deleteLeft();
                                n = crd.getData().toString();
                                gave3++;
                                break;
                        }
                        list.add(n);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    synchronized (clients) {
                        // muestra el mensaje de usuarios conectados y estadisticas de cartas por jugador
                        String winner = "";
                        if (gave1 >= 13 && list1.isEmpty()) {
                            winner = "El ganador es: Jugador 1";
                        } else if (gave2 >= 13 && list2.isEmpty()) {
                            winner = "El ganador es: Jugador 2";
                        } else if (gave3 >= 14 && list3.isEmpty()) {
                            winner = "El ganador es: Jugador 3";
                        }
                        for (Session client : clients) {
                            client.getBasicRemote().sendText(showPlayersConected(clients.size()) + "," + !list1.isEmpty()
                                    + "," + !list2.isEmpty() + "," + !list3.isEmpty() + "," + list1.forceSize() + "," + list2.forceSize()
                                    + "," + list3.forceSize() + "," + n + "," + list.forceSize() + "," + turn + "," + my + "," + "" + "," + winner);
                        }
                        //System.out.print("\"" + list.forceSize() + " \"");
                        //Nos muestra el orden de las cartas por mazo
                        list.show();
                        //System.out.print("\"" + list1.forceSize() + " \"");
                        list1.showFromLeft();
                        //System.out.print("\"" + list2.forceSize() + " \"");
                        list2.showFromLeft();
                        //System.out.print("\"" + list3.forceSize() + " \"");
                        list3.showFromLeft();
                    } 
                }
            }
        } else if (message.equals("killAll")) {
            returnCards();
            if (clients.size() == 3) {
                //System.out.println(list.forceSize());
                //System.out.println(list1.forceSize());
                //System.out.println(list2.forceSize());
                //System.out.println(list3.forceSize());
                for (Player pl : players) {
                    distribution(pl);
                }
                synchronized (clients) {
                    // muestra el mensaje de usuarios conectados y estadisticas de cartas por jugador
                    for (Session client : clients) {
                        client.getBasicRemote().sendText(showPlayersConected(clients.size()) + "," + !list1.isEmpty()
                                + "," + !list2.isEmpty() + "," + !list3.isEmpty() + "," + list1.forceSize() + "," + list2.forceSize()
                                + "," + list3.forceSize() + "," + "" + "," + list.forceSize() + "," + turn + "," + my + "," + "" + "," + ""
                                        + "," + "kill");
                    }
                }
                my = 1;
                //System.out.println(list.forceSize() + "    " + list1.forceSize() + "    " + list2.forceSize() + "    " + list3.forceSize());
            }
        } else {
            if (message.equals("turnoIniciado") && my == ptr) {//Inicia el turno ;00| Verificando que la sesion haga match con el numero de turno
                synchronized (clients) {
                    //System.out.println(my + " ---- " + ptr);
                    turn = 0;
                    turnCountdown = "Rol";
                    clicker = session.getId();
                    my++;
                    if (my == 4) {
                        my = 1;

                    }
                    //System.out.println(message);
                    // este for es para hacer lo mismo en las sesiones activas
                    for (Session client : clients) {
                        // envia este String separado por comas al java script en cada sesion activa
                        client.getBasicRemote().sendText(
                                // mensaje a mostrar de cantidad de jugadores conectados
                                showPlayersConected(clients.size()) + ","
                                //
                                + !list1.isEmpty() + ","
                                + !list2.isEmpty() + ","
                                + !list3.isEmpty() + ","
                                // tamaño lista1        
                                + list1.forceSize() + ","
                                + // tamaño lista2
                                list2.forceSize() + ","
                                + // tamaño lista3        
                                list3.forceSize() + ","
                                + // numero de carta        
                                message + "," + list.forceSize() + "," + turn + "," + "" + "," + over + "," + turnCountdown); //+ "," + playerWinner + "," + playerWinnerID);
                    }
                    over++;//ID que juega
                    if (over == 10) {
                        over = 0;//Reinicia a 0
                    }
                }
            }
        }
    }
    // unicamente retorna la cantidad de jugadores conectados

    public int fool(int player) {
        int playerButton = 0;
        Node crd;
        //System.out.println(list.forceSize());
        switch (player) {
            case 1:
                //      System.out.println(list1.forceSize());
                while (!list.isEmpty()) {
                    crd = list.delete();
                    list1.addRight(crd.getData());
                }
                //    System.out.println(list1.forceSize());
                //System.out.println("player one size: "+ size);
                playerButton = 1;
                break;
            case 2:
                //  System.out.println(list2.forceSize());
                while (!list.isEmpty()) {
                    crd = list.delete();
                    list2.addRight(crd.getData());
                }
                //System.out.println(list2.forceSize());
                //System.out.println("player two");
                playerButton = 2;
                break;
            case 3:
                //System.out.println(list3.forceSize());
                while (!list.isEmpty()) {
                    crd = list.delete();
                    list3.addRight(crd.getData());
                }
                //System.out.println(list3.forceSize());
                //System.out.println("player three");
                playerButton = 3;
                break;
        }
        //System.out.println(list.forceSize());
        //System.out.println(list.forceSize());
        over = 0;
        return playerButton;
    }

    public String showPlayersConected(int i) {
        String message;
        switch (i) {
            case 1:
                message = "Un jugador conectado";
                break;
            case 2:
                message = "Dos jugadores conectados";
                break;
            case 3:
                message = "Tres jugadores conectados";
                break;
            default:
                message = "...";
                break;
        }
        return message;
    }

    public void returnCards() {
        //System.out.print("**********");
        com.byteslounge.dbl.Node crt;
        while (!list1.isEmpty()) {
            crt = list1.deleteLeft();
            list.add(crt.getData().toString());
        }
        while (!list2.isEmpty()) {
            crt = list2.deleteLeft();
            list.add(crt.getData().toString());
        }
        while (!list3.isEmpty()) {
            crt = list3.deleteLeft();
            list.add(crt.getData().toString());
        }
        over = 0;
        my = 1;
    }

    private void distribution(Player player) {//Distribuye Cartas en base al número de jugadores
        
        int size = 13;
        if (list1.isEmpty() && list2.isEmpty() && list3.isEmpty()) {
            individualCards(size, list1);
            player.setCards(1);
            gave1 = 0;
        } else if (!list1.isEmpty() && list2.isEmpty() && list3.isEmpty()) {
            individualCards(size, list2);
            player.setCards(2);
            gave2 = 0;
        } else if (!list1.isEmpty() && !list2.isEmpty() && list3.isEmpty()) {
            size = 14;
            individualCards(size, list3);
            player.setCards(3);
            gave3 = 0;
        } else {//Truena sapo, cuidado
            // regresar cartas del mazo al jugador que perdio o que cerro session
            if (players.size() == 2) {
                if (list1.isEmpty()) {
                    individualCards(list.forceSize(), list1);
                    player.setCards(1);
                    return;
                } else if (list2.isEmpty()) {
                    individualCards(list.forceSize(), list2);
                    player.setCards(2);
                    return;
                } else {
                    individualCards(list.forceSize(), list3);
                    player.setCards(3);
                    return;
                }
            }
            if (players.size() == 1) {
                if (list1.isEmpty()) {
                    individualCards(list.forceSize() / 2, list1);
                    player.setCards(1);
                } else if (list2.isEmpty()) {
                    individualCards(list.forceSize() / 2, list2);
                    player.setCards(2);
                } else {
                    individualCards(list.forceSize() / 2, list3);
                    player.setCards(3);
                }
            }
        }
    }

    public void individualCards(int size, DoublyLinkedList dlist) {
        Node crd;
        for (byte i = 0; i < size; i++) {
            crd = list.delete();
            dlist.addLeft(crd.getData().toString());
        }
    }

    // se ejecuta unicamente cuando un usuario ingresa a la url del proyecto y crea una nueva session
    @OnOpen
    public void onOpen(Session session) throws IOException {
        // este if es para que cuando se conecte un nuevo usuario y ya hay 3 conectados, no haga nada
        if (clients.size() == 3) {
            //synchronized (clients) {
            // muestra el mensaje de usuarios conectados y estadisticas de cartas por jugador
            //for (Session client : clients) {
            session.getBasicRemote().sendText("Ufff lamento decirte que hay 3 jugadores  "
                    + "disfrutando de una grandiosa partida sin ti" + "," + list1.isEmpty()
                    + "," + list2.isEmpty() + "," + list3.isEmpty() + "," + "" + "," + ""
                    + "," + "" + "," + "" + "," + "" + "," + "");
            //}
            //}
        }
        if (clients.size() != 3) {
            // agrega la session al hash set
            clients.add(session);
            // se instancia un nuevo objeto tipo player
            Player player = new Player(session.getId(), session);
            // se revuelven las cartas del mazo solo cuando inicia session el primer jugador
            if (temp.size() == 40) {
                fillCards();
            }
            // distribuye las cartas correspondientes a cada jugador
            distribution(player);
            // agrega el objeto jugador al ArrayList
            players.add(player);
            synchronized (clients) {
                // muestra el mensaje de usuarios conectados y estadisticas de cartas por jugador
                for (Session client : clients) {
                    client.getBasicRemote().sendText(showPlayersConected(clients.size()) + "," + !list1.isEmpty()
                            + "," + !list2.isEmpty() + "," + !list3.isEmpty() + "," + list1.forceSize() + "," + list2.forceSize()
                            + "," + list3.forceSize() + "," + "" + "," + list.forceSize() + "," + turn + "," + my + "" + ",");
                }
            }
            //System.out.println(list.forceSize() + "    " + list1.forceSize() + "    " + list2.forceSize() + "    " + list3.forceSize());
        }
    }

    // cierra la session por jugador que sale del juego
    @OnClose
    public void onClose(Session session) throws IOException {
        // Remove session from the connected sessions set
        int i = 0;
        int k = players.size();
        boolean control = false;
        //System.out.println("sesiones: " + clients.size() + "     jugadores: " + players.size());
        for (byte j = 0; j < k; j++) {
            Player pl = players.get(j);
            //System.out.println(pl.getId() + "    " + pl.getCards() + "  " + pl.getpSession());
            // aun esta pendiente regresar cartas al mazo general
            if (session == pl.getpSession()) {
                i = pl.getCards();
                switch (pl.getCards()) {//Regresan las cartas al mazo
                    case 1:
                        while (!list1.isEmpty()) {
                            list.add(list1.deleteLeft().getData().toString());
                        }
                        break;
                    case 2:
                        while (!list2.isEmpty()) {
                            list.add(list2.deleteLeft().getData().toString());
                        }
                        break;
                    case 3:
                        while (!list3.isEmpty()) {
                            list.add(list3.deleteLeft().getData().toString());
                        }
                        break;
                }
                control = true;
                players.remove(pl);
                //System.out.println("**");
                //System.out.println(list.forceSize() + "    " + list1.forceSize() + "    " + list2.forceSize() + "    " + list3.forceSize());
                
                k--;
            }
        }
        // remueve sesion del hash set
        if (control) {
            clients.remove(session);
            synchronized (clients) {
                for (Session client : clients) {
                    client.getBasicRemote().sendText("Jugador " + i + " abandonó la partida" + "," + !list1.isEmpty()
                            + "," + !list2.isEmpty() + "," + !list3.isEmpty() + "," + list1.forceSize() + "," + list2.forceSize()
                            + "," + list3.forceSize() + "," + "" + "," + list.forceSize() + "," + turn);
                }
            }
        }
    }
}
